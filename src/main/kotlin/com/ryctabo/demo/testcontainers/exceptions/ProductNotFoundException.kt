package com.ryctabo.demo.testcontainers.exceptions

class ProductNotFoundException(val id: String): RuntimeException()