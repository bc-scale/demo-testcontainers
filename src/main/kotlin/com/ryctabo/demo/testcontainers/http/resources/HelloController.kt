package com.ryctabo.demo.testcontainers.http.resources

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("hello")
class HelloController {

    @GetMapping
    fun sayHello(name: String?): String {
        return name?.let { "Hi $name!" } ?: "Hello world!"
    }
}