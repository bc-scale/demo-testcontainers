package com.ryctabo.demo.testcontainers.http.handlers

import com.ryctabo.demo.testcontainers.exceptions.ProductNotFoundException
import com.ryctabo.demo.testcontainers.http.responses.Problem
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.ServletWebRequest
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ProductNotFoundErrorHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(ProductNotFoundException::class)
    fun handleConflict(
        exception: ProductNotFoundException,
        webRequest: WebRequest
    ): ResponseEntity<Any> {
        val problem = Problem(
            title = "Product Not Found",
            description = "Product with ID ${exception.id} was not found",
            ref = "https://httpstatuses.org/404",
            status = HttpStatus.NOT_FOUND.value(),
            path = (webRequest as ServletWebRequest).request.servletPath,
            code = "PRODUCT_NOT_FOUND"
        )
        return handleExceptionInternal(exception, problem, HttpHeaders(), HttpStatus.NOT_FOUND, webRequest)
    }
}