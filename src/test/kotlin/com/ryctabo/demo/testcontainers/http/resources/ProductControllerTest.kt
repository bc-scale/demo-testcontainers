package com.ryctabo.demo.testcontainers.http.resources

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.ryctabo.demo.testcontainers.AbstractIntegrationTest
import com.ryctabo.demo.testcontainers.persistence.document.Product
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

internal class ProductControllerTest : AbstractIntegrationTest() {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var mongoTemplate: MongoTemplate

    @Value("classpath:database/products-data.json")
    private lateinit var dataSet: Resource

    @Value("classpath:mocks/requests/create-product-request.json")
    private lateinit var productRequest: Resource

    @BeforeEach
    fun setUp() {
        val mapper = jacksonObjectMapper().registerModule(JavaTimeModule())
        val javaType = mapper.typeFactory.constructCollectionType(List::class.java, Product::class.java)
        val products: List<Product> = mapper.readValue(dataSet.inputStream, javaType)
        mongoTemplate.insertAll(products)
    }

    @AfterEach
    fun tearDown() {
        mongoTemplate.dropCollection(Product::class.java)
    }

    @Test
    fun `when make a GET request to product resource, the response must be an array and the status is Ok`() {
        mockMvc.perform(get("/products"))
            .andExpect(status().isOk)
            .andExpect(jsonPath("$").isArray)
            .andExpect(jsonPath("$.length()", `is`(3)))
            .andExpect(jsonPath("$[0].name", containsString("Samsung Galaxy")))
            .andExpect(jsonPath("$[0].price", `is`(801.89)))
            .andExpect(jsonPath("$[0].createdAt").isString)
    }

    @Test
    fun `when make a POST request to product resource, the response must be 201 and the body must have an id`() {
        mockMvc.perform(
            post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(productRequest.inputStream.readAllBytes())
        )
            .andExpect(status().isCreated)
            .andExpect(header().exists("Location"))
            .andExpect(jsonPath("$.id").isNotEmpty)
    }

    @Test
    fun `when make a GET request to product with ID, the response must be Ok`() {
        mockMvc.perform(get("/products/{id}", "626672cbb1126e182614e564"))
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.name", containsString("Samsung Galaxy S22+")))
            .andExpect(jsonPath("$.description", containsString("creative expression")))
            .andExpect(jsonPath("$.price").isNumber)
            .andExpect(jsonPath("$.createdAt").isString)
    }

    @Test
    fun `when make a GET request to product with ID, the response must be not found`() {
        mockMvc.perform(get("/products/{id}", "6345e78a305a1f7b3d1dc6d5"))
            .andExpect(status().isNotFound)
            .andExpect(jsonPath("$.title", `is`("Product Not Found")))
            .andExpect(
                jsonPath(
                    "$.description",
                    containsString("Product with ID 6345e78a305a1f7b3d1dc6d5 was not found")
                )
            )
            .andExpect(jsonPath("$.status").isNumber)
            .andExpect(jsonPath("$.code", `is`("PRODUCT_NOT_FOUND")))
    }
}